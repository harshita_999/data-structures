#include<stdio.h>
#include<stdlib.h>
void enqueue();
void dequeue();
void display();

typedef struct queue{
    int data;
    struct queue * link;
}node;

node * front = NULL;
node * rear = NULL;

int main(){
    int choice;
    do{
         printf("1.enqueue\n2.dequeue\n3.display\n4.exit\n");
         scanf("%d",&choice);
         if(1 == choice)
             enqueue();
         if(2 == choice)
             dequeue();
         if(3 == choice)
             display();
    }
 
    while(4 > choice);

    return 0;
}
    


void enqueue(){
    node * p = malloc(sizeof(node));
    int ele;
    printf("Enter the value");
    scanf("%d",&ele);
    p->data = ele;
    p->link = NULL;
    if(front == NULL && rear == NULL){
        front = rear =p;
    }
    else{
        rear->link = p;
        rear =p;
    }
}

void dequeue(){
    if(front == NULL && rear == NULL){
        printf("Queue Underflow");
    }
    else{
        if(front == rear ){
            printf("element deleted is %d",front->data);
            node * t = front;
            front = rear = NULL;
            free(t);
        }
        else{
            printf("Element deleted is %d",front->data);
            node * t = front;
            front = front -> link;
            free(t);
        }
    }

}


void display(){
    node * t = front;
    if(front == NULL && rear == NULL){
        printf("no elements in queue");
    }
    else{
        while(t != NULL){
            printf("%d",t->data);
        }
    }
}



